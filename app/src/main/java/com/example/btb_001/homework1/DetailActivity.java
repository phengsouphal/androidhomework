package com.example.btb_001.homework1;

import android.content.Intent;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    String Name,Phone,Classroom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        TextView textView2=(TextView)findViewById(R.id.textname);
        TextView textView3=(TextView)findViewById(R.id.textphone);
        TextView textView4=(TextView)findViewById(R.id.textclass);

      Info info=(Info) getIntent().getSerializableExtra("Student");

      textView2.setText(info.getName());
        textView3.setText(info.getPhone());
        textView4.setText(info.getClassroom());

    }

    public void btngo(View view){
        Intent i=new Intent(this,MainActivity.class);
        startActivity(i);
    }
}
