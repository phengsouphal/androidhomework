package com.example.btb_001.homework1;

import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText Name,Phone,Classroom;
    Button btncall;
    String phonecall;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Name=(EditText)findViewById(R.id.txtname);
        Phone =(EditText)findViewById(R.id.txtphone);
        Classroom=(EditText)findViewById(R.id.txtclass);
        btncall=(Button)findViewById(R.id.btncall);

    }

    public void getDetailActivity(View view) {

        Intent i=new Intent(this,DetailActivity.class);

        String name=Name.getText().toString();
        String phone=Phone.getText().toString();
        String classroom=Classroom.getText().toString();

        Info info = new Info(name,phone,classroom);
        i.putExtra("Student",info);
        startActivityForResult(i,2);


    }

    public void makePhoneCall(View view){
    Intent i=new Intent(Intent.ACTION_DIAL);
//    i.setData(Uri.parse("tel:",))
    }


}
